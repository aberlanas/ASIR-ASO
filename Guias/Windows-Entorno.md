# Entorno de Trabajo Windows 

## Instalación de PowerShell 

En entornos MS Windows es conveniente (como siempre) estar actualizado, así que vamos a descargarnos
la última versión de PowerShell desde el repositorio de GitHub Oficial:

- [ PowerShell - GitHub ](https://github.com/PowerShell/PowerShell/)

## Instalación de Vim

La página de **freecodecamp** nos ofrece un montón de recursos, y uno de ellos es este simpático manual acerca
de cómo se instala **EL EDITOR** en entornos Windows con PowerShell:

- [ VIM en Windows ](https://www.freecodecamp.org/news/vim-windows-install-powershell/)

## Instalación de Terminal en Windows

Usando la Web o mediante la MS Store, instalad el cliente `Terminal` para MS Windows, que mejora la experiencia
de usuario en Windows de este tipo de herramientas. 

Es conveniente también que lo configuréis para que directamente se lance *PowerShell* que habéis instalado.

## Instalación de Python 
