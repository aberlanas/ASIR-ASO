#!/usr/bin/make -f

#TEMPLATE_TEX_PD="rsrc/templates/pd-nologo-tpl.latex"
# Colors
BLUE= \e[1;34m
LIGHTBLUE= \e[94m
LIGHTGREEN= \e[92m
LIGHTYELLOW= \e[93m

RESET= \e[0m

# Templates 
TEMPLATE_TEX_PD="../rsrc/templates/eisvogel.latex"
PANDOC_OPTIONS="-V fontsize=12pt -V mainfont="../rsrc/sorts-mill-goudy/OFLGoudyStM.otf" --pdf-engine=xelatex "
TEMPLATE_TEX_TASK="../rsrc/templates/eisvogel.latex"

# PDFS
PDF_PATH:=$(shell readlink -f PDFS)

UNIT01_NAME_PATH="Unit01-Docker"
UNIT02_NAME_PATH="Unit02-Shell-Python"
UNIT03_NAME_PATH="Unit03-Python"
UNIT04_NAME_PATH="Unit04-LDAP"
UNIT05_NAME_PATH="Unit05-FileSystems"
UNIT06_NAME_PATH="Unit06-Projects"

# Units 
UNIT01_DIR:=$(shell readlink -f $(UNIT01_NAME_PATH))
UD01_FILES := $(wildcard $(UNIT01_DIR)/*.md)

UNIT02_DIR:=$(shell readlink -f $(UNIT02_NAME_PATH))
UD02_FILES:=$(wildcard $(UNIT02_DIR)/*.md)

UNIT03_DIR:=$(shell readlink -f $(UNIT03_NAME_PATH))
UD03_FILES:=$(wildcard $(UNIT03_DIR)/*.md)

UNIT04_DIR:=$(shell readlink -f $(UNIT04_NAME_PATH))
UD04_FILES:=$(wildcard $(UNIT04_DIR)/*.md)

UNIT05_DIR:=$(shell readlink -f $(UNIT05_NAME_PATH))
UD05_FILES:=$(wildcard $(UNIT05_DIR)/*.md)

UNIT06_DIR:=$(shell readlink -f $(UNIT06_NAME_PATH))
UD06_FILES:=$(wildcard $(UNIT06_DIR)/*.md)

# RULES

clean:
	@echo " [${BLUE} * Step : Clean ${RESET}] "
	@echo "${LIGHTBLUE} -- PDFS ${RESET}"
	rm -f PDFS/*.pdf
	rm -f PDFS/*.odt


files:
	@echo " [${BLUE} * Step : Files ${RESET}] "
	@echo "${LIGHTBLUE} * Creating folder [ PDFS ]${RESET}"
	mkdir -p PDFS

prog-didactica: files
	@echo " [ Step : prog-didactica ]"
	@echo " * [ PDF ] : Programacion Didactica ..."
	
	@cd ProgramacionDidactica/ && pandoc --template $(TEMPLATE_TEX_PD) $(PANDOC_OPTIONS) -o $(PDF_PATH)/ProgramacionDidactica_ASO.pdf ./PD_*.md

	@echo " * [ ODT ] : Programacion Didactica ..."
	@cd ProgramacionDidactica/ && pandoc -o $(PDF_PATH)/ProgramacionDidactica_ASO.odt ./PD_*.md 
	
	@echo " * [ PDF Result ] : $(PDF_PATH)/ProgramacionDidactica_ASO.pdf"
	evince $(PDF_PATH)/ProgramacionDidactica_ASO.pdf

propuesta-didactica: files
	@echo " [ Step : propuesta-didactica ]"
	@echo " * [ PDF ] : Propuesta Didactica ..."
	
	@echo " * [ Preparando ficheros ]"
	@echo " *   -- Subconjunto de la Programacion"

	@cd PropuestaDidactica/ && pandoc --template $(TEMPLATE_TEX_PD) $(PANDOC_OPTIONS) -o $(PDF_PATH)/PropuestaDidactica_ASO.pdf ./PD_*.md


unit-01: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} UD 01 - ${UNIT01_NAME_PATH} ${RESET}] "

	@for f in $(UD01_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT01_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done

unit-02: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} UD 02 - ${UNIT03_NAME_PATH} ${RESET}] "

	@for f in $(UD02_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT02_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done	

unit-03: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "

	@echo " [${LIGHTGREEN} UD 03 - ${UNIT03_NAME_PATH} ${RESET}] "


	@for f in $(UD03_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT03_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done	

unit-04: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "

	@echo " [${LIGHTGREEN} UD 04 - ${UNIT04_NAME_PATH} ${RESET}] "


	@for f in $(UD04_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT04_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done	


unit-05: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "

	@echo " [${LIGHTGREEN} UD 05 - ${UNIT05_NAME_PATH} ${RESET}] "


	@for f in $(UD05_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT05_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done	

unit-06: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "

	@echo " [${LIGHTGREEN} UD 06 - ${UNIT06_NAME_PATH} ${RESET}] "


	@for f in $(UD06_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT06_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done	


