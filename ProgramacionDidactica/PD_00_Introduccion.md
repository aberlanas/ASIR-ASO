---
title: Programación Didáctica ASO - Curso 24-25
subtitle: "Curso 2024-2025"
date: \today
author: Angel Berlanas Vicente - IES La Sénia
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
abstract: |
          Programación didáctica para el segundo curso del Grado Superior de Admininstración de Sistemas Informáticos y Redes. 
          Impartida en el IES La Sénia.
          Curso 2024-2025.
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

\newpage

\tableofcontents

