\newpage
# Descripción del Ciclo

Este ciclo formativo (ASIR) está dividido en 11 módulos profesionales
(+2 ampliaciones de módulos susceptibles de impartirse en inglés), como
unidades coherentes de formación, que deben ser superados para obtener la
titulación.

## Primer curso

* Planificación y administración de redes.
* Implantación de sistemas operativos.
* Fundamentos de hardware.
* Gestión de bases de datos.
* Lenguajes de marcas y sistemas de gestión de información.
* Formación y orientación laboral.
* Horario reservado para el módulo impartido en inglés.

## Segundo curso

* Administración de sistemas operativos.
* Servicios de red e Internet.
* Implantación de aplicaciones web.
* Administración de sistemas gestores de bases de datos.
* Seguridad y alta disponibilidad.
* Empresa e iniciativa emprendedora.
* Proyecto de administración de sistemas informáticos en red.
* Horario reservado para el módulo impartido en inglés.
* Formación en Centros de Trabajo.


La duración establecida para este ciclo es de 2000 horas, incluida la
formación en centros de trabajo. Estas 2000 horas se dividen en seis trimestres
que a su vez están agrupados en dos períodos anuales lectivos. Los cinco
primeros trimestres se desarrollan en el centro educativo y el sexto se hará en
un centro de trabajo, completando así la formación del alumnado.

Las ocupaciones que podrían desempeñar (a modo de ejemplo) son:

 * a) Técnico en administración de sistemas.
 * b) Responsable de informática.
 * c) Técnico en servicios de Internet.
 * d) Técnico en servicios de mensajería electrónica.
 * e) Personal de apoyo y soporte técnico.
 * f) Técnico en teleasistencia.
 * g) Técnico en administración de base de datos.
 * h) Técnico de redes.
 * i) Supervisor de sistemas.
 * j) Técnico en servicios de comunicaciones.
 * k) Técnico en entornos web.

