\newpage
# Descripción del Módulo
Dentro de este Ciclo, la presente programación didáctica hace referencia
al módulo de *Administración de Sistemas Operativos* (**ASO**).

De las unidades de competencia del *Catálogo Nacional de Cualificaciones Profesionales* incluidas en el título, las unidades de competencia asociadas al módulo **ASO** son:

* **UC0484_3**: Administrar los dispositivos hardware del sistema.
* **UC0485_3**: Instalar, configurar y administrar el software de base y de aplicación del sistema.
* **UC0495_3**: Instalar, configurar y administrar el software para gestionar un entorno web.
* **UC0490_3**: Gestionar servicios en el sistema informático.

La formación del módulo contribuye a alcanzar los objetivos generales a), b), n), ñ), o) y q) del ciclo formativo, y las competencias profesionales, personales y sociales a), l), m), n), ñ), o), q), r) y s) del título.

El módulo de **ASO** presenta las siguientes características:

* Tiene una duración de 120 horas.
* Pertenece al 2do curso del ciclo ASIR
* Se imparte durante 6 horas por semana.

