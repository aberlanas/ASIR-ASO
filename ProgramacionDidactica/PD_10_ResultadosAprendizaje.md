\newpage
# Resultados de aprendizaje y criterios de evaluación

Los resultados de aprendizaje son el conjunto de saberes que el alumnado ha de construir y organizar en sus esquemas de conocimiento, las
habilidades cognitivas y destrezas motrices que deberán ser desplegadas en sus actuaciones, así como las actitudes que es necesario ir inculcando y
desarrollando en el alumnado. Estos saberes, aplicados en un entorno laboral, se convertirán en competencias laborales.
De acuerdo con la normativa vigente, los objetivos específicos están expresados en términos de resultados de aprendizaje. En el apartado de los
criterios de evaluación estos están relacionados con los mismos.

Los resultados de aprendizaje ligados a este módulo están especificados en el BOE, y son los siguientes:

\newpage

## Resultado 01

Administra el servicio de directorio interpretando especificaciones e integrándolo en una red.

### Criterios de evaluación

| Identificador | Criterio                                                                |
| -:            | -----------                                                             |
| 01.a          | Se han identificado la función, los elementos y las estructuras lógicas del servicio de directorio.|
| 01.b          | Se ha determinado y creado el esquema del servicio de directorio.|
| 01.c          | Se ha realizado la instalación del servicio de directorio en el servidor.|
| 01.d          | Se ha realizado la configuración y personalización del servicio de directorio.|
| 01.e          | Se ha integrado el servicio de directorio con otros servicios.|
| 01.f          | Se han aplicado filtros de búsqueda en el servicio de directorio.|
| 01.g          | Se ha utilizado el servicio de directorio como mecanismo de acreditación centralizada de los usuarios en una red.|
| 01.h		| Se ha realizado la configuración del cliente para su integración en el servicio de directorio.|
| 01.i		| Se han utilizado herramientas gráficas y comandos para la administración del servicio de directorio.|
| 01.j		| Se ha documentado la estructura e implantación del servicio de directorio.|

---

## Resultado 02 

Administra procesos del sistema describiéndolos y aplicando criterios de seguridad y eficiencia. 

### Criterios de evaluación:
| Identificador | Criterio                                                               |
| -:           | --------                                                               |
| 02.a         | Se han descrito el concepto de proceso del sistema, tipos, estados y ciclo de vida.                  |
| 02.b         | Se han utilizado interrupciones y excepciones para describir los eventos internos del procesador.                 |
| 02.c         | Se ha diferenciado entre proceso, hilo y trabajo.                              |
| 02.d         | Se han realizado tareas de creación, manipulación y terminación de procesos.                           |
| 02.e         | Se ha utilizado el sistema de archivos como medio lógico para el registro e identificación de los procesos del sistema.             |
| 02.f         | Se han utilizado herramientas gráficas y comandos para el control y seguimiento de los procesos del sistema. |
| 02.g         | Se ha comprobado la secuencia de arranque del sistema, los procesos implicados y la relación entre ellos.                                                                      |
| 02.h		| Se han tomado medidas de seguridad ante la aparición de procesos no identificados.|
| 02.i		| Se han documentado los procesos habituales del sistema, su función y relación entre ellos.|
---

## Resultado 03

Gestiona la automatización de tareas del sistema, aplicando criterios de eficiencia y utilizando comandos y herramientas gráficas. 

### Criterios de evaluación

| Identificador | Criterio                                                                                |
| -:           | --------                                                                                |
| 03.a         |  Se han descrito las ventajas de la automatización de las tareas repetitivas en el sistema.|
| 03.b         |  Se han utilizado los comandos del sistema para la planificación de tareas.|
| 03.c         |  Se han establecido restricciones de seguridad.|
| 03.d         |  Se han realizado planificaciones de tareas repetitivas o puntuales relacionadas con la administración del sistema. |
| 03.e         |  Se ha automatizado la administración de cuentas.|
| 03.f         | Se han instalado y configurado herramientas gráficas para la planificación de tareas.|
| 03.g         |  Se han utilizado herramientas gráficas para la planificación de tareas.|
| 03.h         |  Se han documentado los procesos programados como tareas automáticas.|

---

## Resultado 04

Administra de forma remota el sistema operativo en red valorando su importancia y aplicando criterios de seguridad.

### Criterios de evaluación

| Identificador | Criterio                                                                                                        |
| -:           | --------                                                                                                        |
| 04.a         | Se han descrito métodos de acceso y administración remota de sistemas. |
| 04.b         |  Se ha diferenciado entre los servicios orientados a sesión y los no orientados a sesión. |
| 04.c         | Se han utilizado herramientas de administración remota suministradas por el propio sistema operativo.|
| 04.d         | Se han instalado servicios de acceso y administración remota.| 
| 04.e         | Se han utilizado comandos y herramientas gráficas para gestionar los servicios de acceso y administración remota. |
| 04.f         | Se han creado cuentas de usuario para el acceso remoto.  |
| 04.g         | Se han realizado pruebas de acceso y administración remota entre sistemas heterogéneos. |
| 04.h         | Se han utilizado mecanismos de encriptación de la información transferida.|
| 04.i         | Se han documentado los procesos y servicios del sistema administrados de forma remota. |

---

## Resultado 05

Administra servidores de impresión describiendo sus funciones e integrándolos en una red.

### Criterios de evaluación

| Identificador | Criterio                                                                                                            |
| -:           | --------                                                                                                            |
| 05.a         |  Se ha descrito la funcionalidad de los sistemas y servidores de impresión. |
| 05.b         | Se han identificado los puertos y los protocolos utilizados. |
| 05.c         | Se han utilizado las herramientas para la gestión de impresoras integradas en el sistema operativo. |
| 05.d         | Se ha instalado y configurado un servidor de impresión en entorno Web.	 |
| 05.e         | Se han creado y clasificado impresoras lógicas. |
| 05.f         | Se han creado grupos de impresión. |
| 05.g         | Se han gestionado impresoras y colas de trabajos mediante comandos y herramientas gráficas. |
| 05.h		|  Se han compartido impresoras en red entre sistemas operativos diferentes.|
| 05.i		| Se ha documentado la configuración del servidor de impresión y de las impresoras creadas.|

### Cambio y ampliación

Dentro de la actualización y adaptación de los contenidos del ciclo a las novedades relativas al mundo laboral y a las nuevas 
nuevas necesidades de los profesionales del sector, se ha ampliado el contenido de la materia con servicios de almacenamiento 
de archivos en la nube, reduciendo al mínimo la parte de impresión (que cada vez está en más desuso debido a que las empresas imprimen
cada vez menos).

## Resultado 06

Integra sistemas operativos libres y propietarios, justificando y garantizando su interoperabilidad.

### Criterios de evaluación
 
| Identificador | Criterio                                                                                                            |
| -:           | --------                                                                                                            |
| 06.a         |  Se ha identificado la necesidad de compartir recursos en red entre diferentes sistemas operativos. |
| 06.b         | Se han establecido niveles de seguridad para controlar el acceso del cliente a los recursos compartidos en red. |
| 06.c         | Se ha comprobado la conectividad de la red en un escenario heterogéneo. |
| 06.d         | Se ha descrito la funcionalidad de los servicios que permiten compartir recursos en red.	 |
| 06.e         | Se han instalado y configurado servicios para compartir recursos en red.|
| 06.f         | Se ha comprobado el funcionamiento de los servicios instalados. |
| 06.g         | Se ha trabajado en grupo para acceder a sistemas de archivos e impresoras en red desde equipos con diferentes sistemas operativos. |
| 06.h		|  Se ha documentado la configuración de los servicios instalados. |

## Resultado 07

Utiliza lenguajes de guiones en sistemas operativos, describiendo su aplicación y administrando servicios del sistema operativo.

### Criterios de evaluación

| Identificador | Criterio                                                                                                            |
| -:           | --------                                                                                                            |
| 07.a         |  Se han utilizado y combinado las estructuras del lenguaje para crear guiones.|
| 07.b         | Se han utilizado herramientas para depurar errores sintácticos y de ejecución.|
| 07.c         | Se han interpretado guiones de configuración del sistema operativo.|
| 07.d         | Se han realizado cambios y adaptaciones de guiones del sistema.	 |
| 07.e         | Se han creado y probado guiones de administración de servicios.|
| 07.f         |  Se han creado y probado guiones de automatización de tareas.|
| 07.g         |Se han implantado guiones en sistemas libres y propietarios. |
| 07.h		|  Se han consultado y utilizado librerías de funciones.|
| 07.i	| Se han documentado los guiones creados.|
---
