\newpage 

# Competencias profesionales, personales y sociales

Las competencias, de acuerdo a la Ley Orgánica 5/2002 de 19 de junio, de las cualificaciones y de la formación profesional aparecen definidas como:
“El conjunto de conocimientos y capacidades que permitan el ejercicio de la actividad profesional conforme a las exigencias de la producción y el empleo”.
Este conjunto de capacidades no se limita a la simple ejecución de tareas y a los conocimientos adquiridos, sino que también involucra una combinación de
atributos con respecto al saber, saber hacer, saber estar y saber ser.

Las competencias asociadas son:

| Identificador | Competencia |
| -: |-----------|
| a | Administrar sistemas operativos de servidor, instalando y configurando el software, en condiciones de calidad para asegurar el funcionamiento del sistema.|
|l  | Administrar usuarios de acuerdo a las especificaciones de explotación para garantizar los accesos y la disponibilidad de los recursos del sistema.
| m |Diagnosticar las disfunciones del sistema y adoptar las medidas correctivas para restablecer su funcionalidad.|
| n | Gestionar y/o realizar el mantenimiento de los recursos de su área (programando y verificando su cumplimiento), en función de las cargas de trabajo y el plan de mantenimiento.|
| ñ|  Efectuar consultas, dirigiéndose a la persona adecuada y saber respetar la autonomía de los subordinados, informando cuando sea conveniente.|
|o | Mantener el espíritu de innovación y actualización en el ámbito de su trabajo para adaptarse a los cambios tecnológicos y organizativos de su entorno profesional.|
|p | Liderar situaciones colectivas que se puedan producir, mediando en conflictos personales y laborales, contribuyendo al establecimiento de un ambiente de trabajo agradable y actuando en todo momento de forma sincera, respetuosa y tolerante.|
| q |Resolver problemas y tomar decisiones individuales, siguiendo las normas y procedimientos establecidos, definidos dentro del ámbito de su competencia.|
|r| Gestionar su carrera profesional, analizando las oportunidades de empleo, autoempleo y de aprendizaje.|
|s | Participar de forma activa en la vida económica, social y cultural con actitud crítica y responsable.|
