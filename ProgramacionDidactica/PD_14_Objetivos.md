\newpage

# Objetivos

Los objetivos son las intenciones educativas que orientan la planificación
educativa y la ejecución de las actividades formativas necesarias para alcanzar
las grandes finalidades educativas explicitadas en nuestro ordenamiento
jurídico. Estos objetivos van a establecer las capacidades generales que se
espera adquieran los alumnos como consecuencia de la intervención educativa
durante el proceso. Constituyen una guía para la planificación del aprendizaje.


## Objetivos generales del ciclo que se imparten en este módulo

La formación del módulo contribuye a alcanzar los objetivos generales  del ciclo formativo:

| Identificador | Objetivo General del Ciclo |
| -: |-----------|
| a | Analizar la estructura del software de base, comparando las características y prestaciones de sistemas libres y propietarios, para administrar sistemas operativos de servidor.|
| b | Instalar y configurar el software de base, siguiendo documentación técnica y especificaciones dadas, para administrar sistemas operativos de servidor.|
| n | Asignar los accesos y recursos del sistema, aplicando las especificaciones de la explotación, para administrar usuarios |
| ñ |Aplicar técnicas de monitorización interpretando los resultados y relacionándolos con las medidas correctoras para diagnosticar y corregir las disfunciones.|
| o |Establecer la planificación de tareas, analizando actividades y cargas de trabajo del sistema para gestionar el mantenimiento.|
| q | Identificar formas de intervención en situaciones colectivas, analizando el proceso de toma de decisiones y efectuando consultas para liderar las mismas.|

