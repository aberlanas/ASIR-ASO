\newpage 

# Contenidos

Los elementos de referencia que se utilizan para alcanzar los resultados
de aprendizaje del módulo son los contenidos.

Planteados a lo largo del desarrollo de las unidades didácticas, aparecerán los diferentes contenidos *conceptuales, procedimentales y*
*actitudinales* que se deben adquirir durante el proceso de enseñanza-aprendizaje (Merrill, 1983).

Los *conceptuales* son aquellos que hacen referencia a los  saberes  “acumulativos” (principios  y datos).

Los *procedimentales* son aquellos que señalan los procesos que están involucrados
en la resolución de un problema; para ello se ponen en juego destrezas,
habilidades y estrategias que los alumnos deben adquirir. Conocer estos
procedimientos implica saber cómo se hace algo y por tanto cuál es la manera
más eficaz para trabajar en una situación o en la resolución de un problema.
Respecto a los contenidos *actitudinales*, estos deben estar presentes
siempre en el proceso educativo, ya que tienen lugar en el propio proceso,
haciendo referencia a los cambios de conducta y a los principios directores de
esta conducta. Algunos de estos contenidos tienen que ver con las normas de
conductas sociales, tales como:

* Trabajo en grupo.
* Tolerancia y respeto.
* Tomar conciencia de la limitación de los recursos y su uso racional (*Green IT*).

Otros, tal y como se ha visto anteriormente en las competencias
personales y sociales, sí que están directamente relacionadas con el módulo:

La Orden 29 de Julio del 2009, por la que se establece el currículo formativo,
establece los contenidos que en este módulo se puede agrupar en varios
bloques temáticos. A la hora de desarrollar esta programación didáctica se han
numerado para poder establecer una relación entre las unidades didácticas que
se presentan y estos contenidos.

\newpage
## Contenidos del currículo

**Aplicación de lenguajes de script en sistemas operativos libres y propietarios:**

- La shell como lenguaje de script.
- Estructuras del lenguaje.
- Script orientado a objetos.
- Creación y depuración de scripts en sistemas propietarios y libres.
- Interpretación de scripts del sistema. Adaptaciones.
- Utilización de extensiones de comandos para tareas de administración.
- Personalización y/o creación de extensiones de comandos específicos.
- Acceso al registro.
- Programación para la gestión del directorio activo.
- Scripts para la administración de cuentas de usuario, procesos y servicios del sistema operativo.
- Documentación de los scripts creados.

**Administración de servicios de directorios**

- Servicio de directorio. Definición, elementos y nomenclatura. LDAP.
- Esquema del servicio de directorio.
- Estructura física del servicio de directorio.
- Estructura lógica del servicio de directorio.
- Funciones del dominio.
- Instalación, configuración y personalización del servicio de directorio.
- Integración del servicio de directorio con otros servicios.
- Filtros de búsqueda.
- Creación de dominios.
- Objetos que administra un dominio: usuarios globales, grupos, equipos entre otros.
- Relaciones de confianza entre dominios.
- Herramientas gráficas de administración del servicio de directorio.
- Directivas de seguridad del dominio. Derechos de usuario.
- Directivas de grupo. Plantillas administrativas.
- Distribución de aplicaciones remotas a usuarios.
- Seguridad en aplicaciones remotas.
- Organización de objetos de un dominio. Unidades organizativas.
- Diseño de unidades organizativas.
- Administración de unidades organizativas. 
- Estrategias de organización. 
- Delegación de la administración. 
- Asignación de directivas en un dominio.
- Documentación del servicio de directorio

**Administración de procesos del sistema:**

- Procesos. Tipos. Estados. Transiciones de estados. Estructura.
- Hilos de ejecución.
- Prioridades. Interrupciones. Excepciones.
- Identificación de procesos. Dependencias. Herramientas gráficas.
- Gestión de los procesos del sistema. Línea de orden. Entorno gráfico.
- Secuencia de arranque del sistema. Demonios.
- Niveles de ejecución del sistema. Cambio.
- Documentación de procesos.

**Información del sistema. Automatización de tareas:**

- Estructura de directorios.
- Búsqueda de información del sistema. Órdenes. Herramientas gráficas.
- Sistema de archivos virtual. Obtención de información del sistema. directorios utilizados.
- Software instalado. Órdenes. Herramientas gráficas.
- Gestión de la información del sistema. Rendimiento. Estadísticas.
- Automatización de tareas del sistema. Herramientas gráficas.
- Configuración de tareas programadas en el servicio de directorio.
- Documentación de las tareas programadas.

**Instalación, configuración y uso de servicios de acceso y administración remota:**

- Terminales en modo texto.
- Escritorio remoto.
- Protocolos de acceso remoto y puertos implicados.
- Servicios de acceso remoto del propio sistema operativo.
- Herramientas gráficas externas para la administración remota.
- Acceso remoto seguro en redes homogéneas y heterogéneas.
- Actualización remota del sistema operativo.
- Comprobación de la seguridad del sistema.

**Administración de servidores de impresión:**

- Puertos y protocolos de impresión.
- Tipos de impresoras para trabajo en red.
- Sistemas de impresión.
- Órdenes para la gestión de impresoras y trabajos.
- Administración de impresoras y servidores de impresión.

**Integración de sistemas operativos en red libres y propietarios:**

- Descripción de escenarios heterogéneos.
- Instalación, configuración y uso de servicios de red para compartir recursos.
- Creación y documentación de mapas de red.
- Configuración de recursos compartidos en red.
- Sistemas de archivos compartidos en red.
- Seguridad de los recursos compartidos en red.
- Protocolos para redes heterogéneas.
- Utilización de redes heterogéneas.
