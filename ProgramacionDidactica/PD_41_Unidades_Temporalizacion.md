\newpage

# Temporalización 

| Unidades | Evaluación | Peso  en la *Evaluación* |
|:--------| :-: | -: | -: |
| Unidad 01 : Docker |  1ª EVA | 20 %| 
| Unidad 02 : Shells y Python| 1ª EVA | 80 %|
|||| ***1ª** EVA* **(30%)**||
| Unidad 03 : LDAP| 2ª EVA | 20 %|
| Unidad 04 : Sistemas de Ficheros | 2ª EVA | 20 %|
| Unidad 05 : Proyecto| 2ª EVA | 60 % | 
|||| ***2ª** EVA* **(70%)**||
