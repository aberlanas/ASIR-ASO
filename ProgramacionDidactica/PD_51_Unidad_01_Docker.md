\newpage 

# Unidad 01 : Docker

## Temporalización

18 Sesiones

## Contenidos

* Virtualización mediante contenedores.
* Imágenes y puesta en marcha.
* Redes Virtuales y puertos.
* Dockerfiles.
* Puntos de montaje y ficheros. 
* Orquestadores.

## Actividades de Enseñanza-Aprendizaje

* Instalación de docker.
* Instalación de imágenes.
* DockerHub.
* Gestión de Redes Virtuales y Puertos.
* Creación de repositorios y Dockerfiles.
* Recursos y sistemas de ficheros.
* Uso de orquestadores.

