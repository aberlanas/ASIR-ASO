\newpage 

# Unidad 02 : Shell y Python

## Temporalización

24 Sesiones

## Contenidos

* En el principio...fué la línea de comandos.
* BASH.
* Estructura sencilla.
* CMD y PowerShell.
* Objetos en PowerShell.
* La CLI como herramienta del Administrador.
* ¿Qué es Python?
* Instalación.
* Estructuras básicas.
* Librerias.
* Funciones.
* Python para Administradores de sistemas.

## Actividades de Enseñanza-Aprendizaje

* Presentación de los diferentes modos de trabajo.
* Creación de pequeños Scripts de Shell.
* Repaso de estructuras básicas de la programación estructurada.
* Conceptos de permisos y ejecución.
* Corregir scripts de Shell que contienen fallos.
* Objetos y acceso a atributos y métodos.
* Instalación de diferentes intérpretes.
* Codificación de diferentes scripts en python.
* Ejecución en diferentes SO.
* Uso de librerias.
* Desarrollo de funciones.
* Investigación acerca de diferentes sistemas de automatización que usan Python como backend.
* Acceso remoto a funciones.
* Bots de Telegram.
* Bindings con Docker.
* Chat en Python.
