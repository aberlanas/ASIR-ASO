\newpage 
# Unidad 03 : LDAP 

## Temporalización

24 Sesiones

## Contenidos

* Conceptos de LDAP.
* Docker y LDAP.
* Estructuras de árbol.
* Grupos y Usuarios.
* Sistema Microsoft y LDAP.

## Actividades de Enseñanza-Aprendizaje

* Diseño de LDAP.
* Instalación y configuración.
* Configuración de clientes.
* Uso de filtros para la gestión de recursos.
* Gestión de los ficheros de log y configuración.
* Instalación y configuración de OpenLDAP y Pgina.
* Integración con los usuarios del centro.
* Creación de un sistema de Login Único.

