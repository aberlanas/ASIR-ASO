\newpage 

# Unidad 04 : Sistemas de Ficheros

## Temporalización

24 Sesiones

## Contenidos

* Puntos de Montaje y Unidades.
* Permisos y Atributos.
* RAIDS y Seguridad.
* Sistemas de ficheros remotos.
* Sistemas de ficheros en la nube.

## Actividades de Enseñanza-Aprendizaje

* Creación de puntos de montaje locales y remotos.
* Uso de NFS, SMB,...
* Instalación y configuración de sistemas de ficheros remotos.
* Actividades de pruebas de seguridad sobre los sistemas de ficheros.
* Instalación y configuración de un Sistema de Owncloud.
* Configuración de plugins sobre Owncloud.

