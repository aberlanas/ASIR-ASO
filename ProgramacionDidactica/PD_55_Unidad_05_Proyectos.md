\newpage 

# Unidad 05 : Proyectos

## Temporalización

24 Sesiones

## Contenidos

* Creación de un proyecto integrador donde se pongan en práctica los contenidos
aprendidos durante el curso.
* Este proyecto deberá estar alojado en Github/GitLab.
* Trabajo diario.

## Actividades de Enseñanza-Aprendizaje

* Diseño e implementación de un proyecto integrador.
* Exposición en público del mismo.
* Creación de una memoria del proyecto.

