---
title: Propuesta Didáctica ASO - Curso 23-24
subtitle: "Curso 2023-2024"
date: \today
author: Angel Berlanas Vicente - IES La Sénia
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
abstract: |
          Propuesta didáctica para el segundo curso del Grado Superior de Admininstración de Sistemas Informáticos y Redes. 
          Impartida en el IES La Sénia.
          Curso 2023-2024.
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

\newpage

\tableofcontents

