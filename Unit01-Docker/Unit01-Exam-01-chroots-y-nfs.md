---
title: Examen 01 - NFS y chroots
subtitle: "Unidad 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia-scp.pdf"
page-background-opacity: 1
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background6-scp.pdf"
---

# Examen (Prueba Verificada por la SCP) 

Estos nuevos Agentes de la *SCP Foundation*, parece que han decidido seguir en el camino del entrenamiento. Vamos a aceptar otro trabajo en *Venus*, donde la Plataforma Ferroviaria *Sub-L* está construyéndose.

En esta ocasión, todo lo aprendido anteriormente se pondrá en juego en los próximos minutos.

# Problema

Uno de nuestros agentes ha sido atacado mientras ejecutaba el script que habíais preparado en el dia de ayer. Las cámaras estan dañadas y parece que el sujeto sabía donde se encontraban, ya que no parece que en *ningún momento* su cara aparezca en las grabaciones. 

No sabemos si el *script* se ha llegado a ejecutar, y lo que es peor, hemos perdido acceso al log del *nfs*...

Necesitamos verificar que el *chroot* que estaba ofreciendo el servicio de NFS en el puerto **40049** está funcionando de manera correcta, en caso de que no lo esté haciendo, que haga las comprobaciones pertinentes (indicadas en la misión anterior) y que lance el servicio.

>NOTA: Recordad que es una buena idea que el servicio de `nfs-kernel-server` esté funcionando también en la máquina real (desde el inicio).

Una vez verificado (sólo las personas **dignas del aprobado**) de manera *elegante*, el script debería crear (usando la salida estándar) una serie de ficheros dentro de la máquina **real** :`/mnt/red-real-ferroviaria/` y además montará el directorio exportado por el *chroot* en la carpeta : `/scp/punto-de-control/`. Si todo va bien, los ficheros recién creados podrán verificarse (`diff`), aquí sería conveniente redirigir salidas de comandos (`date`, por ejemplo).

Para evitar que esto no vuelva a suceder, copiar el fichero de *log del nfs*, así como el fichero de configuración del servicio a la carpeta exportada con el nombre: `/scp/punto-de-control/nfs-scp.YYYYMMDD.HHmm.d/` cada vez que se ejecute el *script*. De esta manera debería de ir quedando un *histórico* de las diferentes ejecuciones del script.

Si consideráis copiar más ficheros o aportar información a esa "*copia*", hacedlo comentando en el script **porqué** lo hacéis.

![SCP-113](imgs/train.png)\

# Misión

* Preparar el entorno.
* Haced el Script.

