---
title: Unit 01 - Chroot
subtitle: "Unit 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background6.pdf"
---

# chroot

En el Laboratorio de **Warren Worringthon III** están llegando demasiados incidentes, más de los que la *ultramente* de Warren es capaz de simultanear. Por eso, está preparando a los nuevos discípul@s una serie de tareas para comprobar si serán capaces de ayudarle en las tareas que estan por venir. 

Usando la Imagen ISO de Xubuntu 24.04, vamos a ver los pasos necesarios en clase para descomprimir el sistema de ficheros y luego hacer un `chroot` para realizar algunas operaciones básicas.

## Tarea 01 : Comandos básicos

A medida que hemos estado en la práctica hemos ido recordando una serie de comandos que pueden ser de utilidad en nuestra labor como futuros sysadmins.

* Comando para obtener a partir de una ruta dada el fichero más grande.
* Comando para descomprimir un fichero `.squashfs`
* Averiguar cual de todos los `squasfhs` de la ISO contiene el `filesystem` necesario para poder interactuar con él.
* Serie de comandos para *montar* los directorios del *kernel* para comenzar las operaciones.

*Aviso a Navegantes*: El Comando para obtener a partir de una ruta dada el fichero más grande presente en ella no parece tan trivial. Vamos a hacer un pequeño script
que nos lo resuelva, y así se repasan conceptos del año pasado.

## Tarea 02 

Crea un Shell script que :

* Comprueba que tienes privilegios de `root`.
* Compruebe que el primer argumento dado es la ruta a una imagen iso de Ubuntu.
* Compruebe que el segundo argumento dado es la ruta a una carpeta válida.
* Si las comprobaciones anteriores han tenido éxito, que monte, descomprima y copie el *filesystem* a la ruta indicada (borrando el contenido de la carpeta de ejecuciones anteriores en caso de que ya existiera).

## Tarea 03 

Una vez dentro del `chroot` vamos a hacer las siguientes operaciones:

* instalar el w3m.
* instalar el vim.
* instalar el tree.

Navegar desde dentro del chroot con el w3m hasta google y buscar **terraformadores de venus** en el buscador.

Entregar una captura de pantalla del resultado.

### T03: Pregunta de refuerzo

¿Cuanto espacio disponible queda en la Máquina Virtual?¿Cuanto ocupa el fichero `.squashfs` correcto?¿Y el contenido una vez descomprimido? Reflexión en clase.

