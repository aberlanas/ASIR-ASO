---
title: Unit 01 - Procesos y Repasos
subtitle: "Unit 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background6.pdf"
---

# Procesos y Servicios

## Tarea 01

Presentar un Shell Script que a partir de una IP que se le indicará como argumento:

1. Verifique el el usuario que ejecuta el script tiene permisos efectivos de `root`.
2. El parámetro IP está presente y es una IP válida.
3. Se puede hacer ping a la misma.
4. Liste los servicios que *posiblemente* esté ejecutando la máquina en esa IP.

## Tarea 02 (Solo si se ha visto redes en docker)

Presentar un Shell Script que al ejecutarse muestre todos los contenedores de docker en marcha con las redes virtuales
que estén usando. Ejemplo de salida:

```shell
| Nombre contenedor | IP Red |
| helloWorld        | 192.168.192.1| 
| test              | 192.168.193.1|

```

## Tarea 03 

Presentar un Shell Script que al ejecutarse muestre:

* Los cuatro procesos que más RAM están consumiendo en el momento de la ejecución.
* Los cuatro procesos que más CPU estan consumiendo en el momento de la ejecución.
* El porcentaje de procesos comunes en ambos listados. 



