---
title: Unit 01 - Creación Redes Virtuales
subtitle: "Unit 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Redes Virtuales y Puertos

## Tarea 01

Usando una imagen de Docker con PHP, monta un pequeño servidor que exponga en la URL: `http://127.0.0.1:6868` el
servicio que *procese* los ficheros de un usuario local en su carpeta : `~/Development/testing/`.

Ese usuario no ha de necesitar permisos especiales para poder trabajar con los ficheros dentro de esa carpeta.

En este ejercicio, la palabra *procesar* hace referencia a que el servidor web, debe ejecutar los ficheros `.php` y
devolver el resultado.

Ejemplo de `index.php` que *podría* desarrollar el desarrollador ^_\^ : 

```php
<?php

// Show all information, defaults to INFO_ALL
phpinfo();

// Show just the module information.
// phpinfo(8) yields identical results.
phpinfo(INFO_MODULES);

?>
```

### Entrega

Captura de pantalla de la ejecución del Docker y del Navegador.

## Tarea 02

Montar lo mismo que en el ejercicio 1, pero usando un `chroot` de Ubuntu, pero en este caso en el puerto `6969`.

### Entrega

Captura de pantalla de la ejecución del Docker y del Navegador.

\newpage
## Tarea 03

Usando *Docker*, monta un servidor de bases de datos (os propongo PostgreSQL o MariaDB) que no tenga interfaz Web ni
gráfica para configurarlo.

Ese servicio Docker ha de estar conectado a dos redes:

* SQLManagerNetwork
* SQLServiceNetwork

Por la interfaz conectada a SQLManagerNetwork será por la que podremos conectarnos al servicio de BBDD de manera
interactiva y crearemos una pequeña base de datos : `BDCine` en la que crearemos esta tabla y la poblaremos con los
datos descritos a continuación:

\newpage

```sql

CREATE TABLE PeliculasSerieB (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    Titulo VARCHAR(255),
    Director VARCHAR(255),
    AñoLanzamiento INT,
    Genero VARCHAR(50),
    Clasificacion VARCHAR(10)
);

INSERT INTO PeliculasSerieB (Titulo, Director, AñoLanzamiento, Genero, Clasificacion)
VALUES
    ('El Monstruo de la Laguna', 'Jack Arnold', 1954, 'Ciencia Ficción', 'B'),
    ('Plan 9 del Espacio Exterior', 'Ed Wood', 1959, 'Ciencia Ficción', 'B'),
    ('El Ataque de los Tomates Asesinos', 'John De Bello', 1978, 'Comedia', 'B'),
    ('Robot Monster', 'Phil Tucker', 1953, 'Ciencia Ficción', 'B'),
    ('La Noche de los Muertos Vivientes', 'George A. Romero', 1968, 'Terror', 'B'),
    ('Carnival of Souls', 'Herk Harvey', 1962, 'Terror', 'B'),
    ('Attack of the 50 Foot Woman', 'Nathan Juran', 1958, 'Ciencia Ficción', 'B'),
    ('El Regreso del Monstruo', 'Bert I. Gordon', 1955, 'Ciencia Ficción', 'B'),
    ('El Cerebro de la Bestia', 'Arthur Crabtree', 1957, 'Ciencia Ficción', 'B'),
    ('La Cosa', 'John Carpenter', 1982, 'Ciencia Ficción', 'B');

```

Una vez el Motor de Base de Datos lo tengamos OK, crearemos otro Docker que también estará conectado a dos "puntos" de
red:

1. Por un lado a la red *SQLServiceNetwork*, por la que se comunicará con nuestro Motor de Base de datos y en este
   Docker montaremos un PHP que se conecte a la la base de datos y que en su `index.php` liste todas las peliculas de la
   base de datos cuyo *Año de Lanzamiento* sea *par*.
2. Por otro lado este, Docker de PHP debe ofrecer sus servicios por el puerto **1666**, en la interfaz de red de la máquina
   anfitriona que deseéis, para que podamos consultar via navegador los resultados obtenidos.


### Entrega 

Realizad un documento en Markdown de todo el proceso de configuración que habéis seguido, con capturas, etc.

![Docker, Docker, Docker](./imgs/docker-is-everywhere.jpeg)\

