---
title: Unit 01 - rsynchroot
subtitle: "Unit 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia-scp.pdf"
page-background-opacity: 1
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background6-scp.pdf"
---

# chroot 

Vamos a empezar a unir conceptos de varios módulos, ya que tanto Iceman como Warren necesitan que los nuevos Agentes de la *SCP Foundation*, estén preparados para enfrentarse a los diferentes sujetos que aparecerán, ya que parece ser que en Venus, uno de los Complejos está siendo "atacado".

# Problema

En el Sótano 12 del Complejo de Desarrollo **Sub-Terra Fecunda**, existe un ordenador que se encarga de realizar copias de seguridad de los diferentes *chroots* que ejecutan procesos de *contención* del Sujeto SCP-113, un peligroso virus-ente informático que de escapar de su *CHROOT-DORADO* sería capaz de tomar conciencia de sí mismo y provocar el fin de las IA (nuestras amigas y aliadas). 

Existen dos *chroots* diferentes, cuyos nombres indican los estados de los pasos de la iluminación: *Yama* y *Niyama*. Estos dos pasos (*chroots*), deben tener instalados los siguientes paquetes (son *exclusivos*, debéis usar el *chroot* original y añadirle el software adecuado ):

* *Yama* : w3m
* *Niyama*: bmon y w3m.

Deberéis de preparar los dos *chroots* para que la tarea del ordenador pueda completarse.

La tarea del ordenador es crear copias de seguridad a voluntad de los *Agentes* de la Fundación, que le indican el `chroot` que desean *backupear* como argumento, pero a veces el **Sujeto SCP-113** es capaz de alterar la hora de los equipos con la intención de destruir el sistema de respaldo (algunos *Agentes* afirman que el **Sujeto SCP-113** es consciente del paso del tiempo), es por eso que no se ha establecido un *crontab*, sino que son los *Agentes especializados* los que ejecutan el script de backup.

Este script de Backup debe ayudar a los *Agentes* a detectar que un ataque de *brecha temporal* ha sido ejecutado por el **Sujeto SCP-113**. ¿Cómo?, pues usando el mayor aliado de la *Fundación*, el Sistema Operativo GNU/LinuX.

Aunque el **Sujeto SCP-113** es capaz de alterar la hora de los equipos, al afectar mediante *ondas heterodoxas* el núcleo de cuarzo presente en el kernel, no es capaz de alterar los ficheros generados mediante **SALIDA ESTANDAR**. Así que lo que haremos será almacenar en un fichero en *Raiz* del `chroot`: `/last-update.scp`, en formato `AAAAMMDDHHMMSS`, el segundo en el que tuvo lugar la última copia. De esta manera, cuando el programa se vuelva a ejecutar para hacer un *backup* de los ficheros modificados, antes de eso comprobará si su hora es anterior a la que almacena el destino... Permitiendo a los agentes detectar esos ataques de *brecha temporal*, mostrando un mensaje de "Alerta".

![SCP-113](imgs/scp-113.jpg)\

# Misión

* Preparar el entorno.
* Haced el Script.
* Ayudar a los agentes contra el **Sujeto SCP-113**.
