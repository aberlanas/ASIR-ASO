---
title: Unit 01 - chroot y nfs
subtitle: "Unit 01"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia-scp.pdf"
page-background-opacity: 1
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background6-scp.pdf"
---

# chroot 

Estos nuevos Agentes de la *SCP Foundation*, parece que han decidido seguir en el camino del entrenamiento. Vamos a aceptar otro trabajo en *Venus*, donde la Plataforma Ferroviaria *Sub-L* está construyéndose.

# Problema

En uno de los dos *chroots* de la tarea anterior necesitamos montar un servicio de compartición de archivos nfs y exponerlo en el puerto **40049** de la máquina real. 
Además montaremos en el interior del chroot la carpeta *real* `/mnt/red-real-ferroviaria/` en el directorio **del chroot** `/exports/red-ferroviaria-exportada/` 
Crea un script que al ejecutarse se asegure de que el chroot está listo, monte los directorios adecuados, compruebe que el puerto es el que toca y ponga en marcha el servicio quedando *attached* al log del nfs.

![SCP-113](imgs/train.png)\

# Misión

* Preparar el entorno.
* Haced el Script.
* Probad a montar el nfs exportado desde la máquina de algun compañer@.
* Preparaos para el Viernes.
