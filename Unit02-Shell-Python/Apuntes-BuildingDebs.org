#+Title: Building Debs
#+Author: Angel Berlanas Vicente
#+Version: 

#+LATEX_COMPILER: xelatex
#+EXPORT_FILE_NAME: ../PDFS/ManualDebs.pdf

#+LATEX_HEADER: \hypersetup{colorlinks=true,linkcolor=blue}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \fancyhead{} % clear all header fields
#+LATEX_HEADER: \pagestyle{fancy}
#+LATEX_HEADER: \fancyhead[R]{2-ASIR: ASO}
#+LATEX_HEADER: \fancyhead[L]{Building Debs}
#+LATEX_HEADER: \usepackage{wallpaper}
#+LATEX_HEADER: \ULCornerWallPaper{0.9}{../rsrc/logos/header_europa.png}
#+LATEX_HEADER: \CenterWallPaper{0.7}{../rsrc/logos/watermark_1.png}
#+LATEX_HEADER: \usepackage{fontspec}


* Inicio

En este documento se describen algunos de los pasos y paquetes necesarios para
la construcción de paquetes /debian/. Es importante recordar que se trata de
una pequeña introducción. El lector que esté interesado en el tema puede avanzar
todo lo que considere en la investigación de este estupendo y divertido sector
del mundo de la Administración de Sistemas.

* Setup

Algunos de los paquetes necesarios para construir el empaquetado son:
  
#+BEGIN_SRC bash
  sudo apt install dpkg-buildpackage
  sudo apt install devscripts
  sudo apt install debhelper
#+END_SRC

También necesitaremos hacer las pruebas en máquinas Debian o derivadas que se encuentren
relativamente limpias de paquetes y configuraciones, con el objetivo de que las pruebas
se realicen en los entornos lo más adecuados posibles para luego poder replicar las situaciones
de instalación, configuración y actualización que se puedan dar.

* Estructura del paquete

A continuación se describe la estructura básica de un /paquete debian/. Durante los ejemplos
nos basamos en que nos hayamos ejecutando las órdenes desde /raiz de paquete/, que es la carpeta
en la que directamente está el directorio ~debian~ y todos los ficheros que podemos instalar
o manipular para la creación de los diferentes /debs/.

** directorio debian

  Dentro de este directorio deben estar los ficheros:

  * changelog
  * compat
  * rules
  * control 
  * copyright

y luego dependiendo de los diferentes binarios que necesitemos, en este ejemplo
nos basamos en el paquete /pepino/:

  * pepino-tool.install
  * pepino-tool.postinst

** raiz de paquete

  pepino-tool.install $\rightarrow$ estructura de directorios que se instalan

* Comandos de construcción y testeo

  Desde raiz de paquete: 

  | Comando                          | Funcion                                |
  |----------------------------------+----------------------------------------|
  | ~dpkg-buildpackage~              | Construye los binarios en ../          |
  | ~sudo apt install ./RUTA_AL_DEB~ | Instala el deb con sus dependencias    |
  | ~fakeroot debian/rules clean~    | Limpia el paquete fuente de temporales |
  | ~dpkg -l nombrePaquete~          | Comprobación de paquetes instalados    |
  |                                  |                                        |

  

  

