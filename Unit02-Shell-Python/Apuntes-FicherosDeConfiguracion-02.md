---
title: Ficheros de Configuración
subtitle: "Unit 02"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Introducción

Vamos a comenzar los apuntes *teóricos* de este módulo con uno de los aspectos más importantes que todo Administrador de
Sistemas debe conocer: **los ficheros de configuración**.

Saber configurar los servicios y aplicaciones son la base de la Administración de Sistemas y Redes.

Recordad que *Todo en los Sistemas Operativos es un fichero* y con esta máxima nos pondremos a trabajar durante esta
unidad con la ayuda de uno de los lenguajes de programación más útiles para el Administrador de Sistemas: **Python**.

# Necesidad

Muchos programas y aplicaciones son configurables desde dentro del propio programa en ejecución, por ejemplo: Añadir una
palabra al diccionario de un editor de textos para que no la marque como "Incorrecta", tener una determinada disposición
de las barras de herramientas en un editor gráfico, ... 

Todo esto son configuraciones que los programadores permiten a los usuarios dentro del uso del software y que forman
parte de lo que veremos que es la configuración de usuario.

Acaba de aparecer en este instante un concepto muy importante:

**Lo que los programadores permiten**

Dentro de la vida de los Administradores de Sistemas, habrá muchas veces que se os pedirá que determinadas aplicaciones
se comporten de una manera, algunas cosas ocurran en determinado momento, etc. Es importante que sepáis que *casi todo*
se puede hacer, pero que dependerá de nuestros conocimientos y de nuestra capacidad de invertir tiempo en la solución
poder satisfacer las necesidades del cliente. 

Dentro del Software Privativo nos encontraremos con muchos más problemas que en el caso del Software Libre, ya que si
por necesidad nos vemos obligados a realizar un cambio que la aplicación no tenga previsto, sencillamente no podremos y
o utilizamos *dirty hacks* o renunciaremos a ello.

\newpage
# Globales y Personales

Pasemos ahora a explicar algunos pequeños conceptos acerca del funcionamiento de los ficheros de configuración.

## Contenido de los ficheros de configuración

Los ficheros de configuración son archivos utilizados para definir y personalizar el comportamiento de servicios, 
aplicaciones y sistemas en un entorno informático. Estos archivos son esenciales para que nosotros como Administradores 
de sistemas podamos ajustar el funcionamiento de las diferentes herramientas y componentes de software de acuerdo 
con las necesidades específicas que tengamos.

Algunos de los tipos de datos que suelen encontrarse en los ficheros de configuración son:

1. **Parámetros de configuración:** Los ficheros de configuración contienen valores que 
   determinan cómo se comporta una aplicación o servicio en particular. Estos valores 
   pueden incluir opciones de seguridad, ajustes de rendimiento, 
   rutas a ficheros, y otros aspectos relacionados con el funcionamiento de la aplicación.
2. **Credenciales:** En algunos casos, los ficheros de configuración pueden almacenar 
   credenciales como contraseñas o claves de acceso. En estos casos se vuelve **fundamental** 
   proteger adecuadamente estos datos confidenciales para evitar posibles brechas de seguridad.
   Vamos a ver algunos ejemplos.
3. **Rutas del sistema:** Los ficheros de configuración pueden especificar las ubicaciones de directorios 
   y ficheros necesarios para que una aplicación o servicio funcione correctamente. 
   Esto incluye rutas para archivos de registro, archivos de configuración adicionales y recursos compartidos.
4. **Configuración de red:** Los ficheros de configuración a menudo incluyen detalles sobre cómo se conecta una aplicación o servicio a la red. 
   Esto puede involucrar direcciones IP, puertos de red, protocolos de comunicación y 
   otros parámetros de configuración relacionados con la conectividad.
5. **Ajustes de seguridad:** La seguridad es una preocupación **crítica** para todos los *sysadmins*. 
   Por lo tanto, los ficheros de configuración pueden contener configuraciones relacionadas *firewalls*, 
   permisos de acceso, autenticación y control de acceso.
6. **Variables de entorno:** Algunas aplicaciones utilizan variables de entorno 
   para ajustar su comportamiento. Estas variables se pueden definir en los ficheros de configuración 
   para facilitar la personalización y la interoperabilidad con otros componentes del sistema.
7. **Ajustes de rendimiento:** Para optimizar el rendimiento de una aplicación o servicio, 
   los ficheros de configuración pueden incluir opciones relacionadas con la asignación de recursos, 
   la memoria disponible o su prioridad para el uso por parte del procesador.
8. **Idioma y localización:** Si una aplicación o servicio es utilizada en diferentes regiones o idiomas, 
   los ficheros de configuración pueden contener configuraciones para adaptar la interfaz 
   de usuario y los mensajes a las preferencias del usuario.
9. **Otras consideraciones:** A lo largo del curso iremos viendo muchos parámetros que se establecen aquí que 
   tal vez no cuadren en ninguna de las categorias que acabamos de describir, pero eso no es importante ahora...

## Tarea 1.1 : Leyendo un fichero con python

Vamos ahora a coger uno de los ficheros de configuración más famosos de todos los sistemas GNU/LinuX 
y vamos a **parsearlo** y a buscar determinados valores en él.

El fichero en cuestión es `/etc/passwd` que contiene la lista de usuarios **locales** del sistema. 
Como primera aproximación, vamos a ir leyéndolo línea a línea buscando aquellos usuarios cuyo **UID**
sea mayor de 100 pero menor 500 (un pequeño ejemplo). 
 
De esos usuarios mostraremos su `login`.

\newpage
```python
#!/usr/bin/python3

import sys
import os

print( " Recorramos el fichero /etc/passwd ")

# En este caso solo como lectura
f = open("/etc/passwd",'r')

# Y ahora entramos en fichero y vamos avanzando
# Una manera elegante de hacerlo es mediante un 
# pequeño bucle While

# Nos apuntamos la linea en la que nos encontramos
numLinea = 0

for linea in f:

    # Incrementamos el numero de la linea
    numLinea=numLinea+1

    #print(" La linea "+str(numLinea)+" es : "+linea.strip())   
    # Porque en la linea de arriba se hace el linea.strip()?

    # Vamos ahora a quedarnos con el campo UID
    arrayLinea = linea.split(':')
    uid = arrayLinea[2]

    # Lo convertimos a entero
    uid = int(uid)

    # Y ahora lo comprobamos
    if uid > 100 and uid < 500:
        print (" * login : "+ arrayLinea[0]+ " ----> "+arrayLinea[2])

f.close()

sys.exit(0)
```

## Tarea 1.2 : Mezclando ficheros

Vamos ahora a hacer en clase un ejercicio...sabemos que en este fichero no se encuentran los passwords de los usuarios.
Pero existe otro que sí que los contiene. Vamos a hacer un script en python que nos muestre para todos los usuarios 
que tienen password establecido en el equipo y que tienen una *shell valida* el login y el password (encriptado).

Vamos a hacer entre todos el comienzo y luego seguiremos individualmente:

# Jerarquias

Próximamente...

# Cerrojos, bloqueos y defaults.

Próximamente...

# Tipos de ficheros de configuración

Próximamente...


