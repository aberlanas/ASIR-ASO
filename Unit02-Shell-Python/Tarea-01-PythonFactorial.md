---
title: Unit 02 - Shell y Python
subtitle: "Version: 0.2"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Script 01 : Python gritao! 

Haz un script de python que si se le pasa un número como argumento, calcule su factorial con las siguientes consideraciones:

- El numero ha de ser un entero (positivo o negativo) entre el **1000** y el **-1000**.
- Si el numero es positivo calcula el factorial de dicho número.
- Si el número es negativo, calcula el factorial de dicho número en valor absoluto y luego aplica una multiplicación por -1 si el número es impar.


## Script 02 : Excepciones!

Crea un segundo script en python que recorra los numeros desde el 1 al 5999 y haga todo lo anterior pero añadiendo:

- Si el numero es menor de 1000 escriba en un fichero llamado : 0000.txt el numero y el resultado (como si fuera el log).
- Si el numero es mayor o igual a 1000 y menor que 6000  escriba en el fichero el número y si ha sido capaz de calcularlo (solamente).
- Si alcanzamos el número más grande que nuestra versión de Python pueda transformar (hacer un casting) a `String` , mostrar un mensaje en pantalla indicando cuál es y salir con error (`try-except`) (esto no debería de ocurrir si llegamos solo hasta el 1000 escribiendo el resultado, pero podéis hacer pruebas de quitando esa comprobación para que falle).

## Script 03 : Mientras puedas factorial!

Crea un tercer script sólo para descubrir cual es el número más grande que tu computadora es capaz de calcular, realiza los pasos del ejercicio anterior pero sin detenerte en el 5999, en el momento que ocurra el desbordamiento (el error), muestra el último número calculado por pantalla.

