---
title: Unit 02 - Python y Entorno
subtitle: "Unit 02"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Script 01 : Donde estoy?

Haz un script de python que realize las siguientes comprobaciones:

- El `id` del usuario es mayor de 1000, en caso contrario detenerse y mostrar un mensaje
  de que el usuario que debe ejecutar el script no ha de ser un usuario especial o administrador.
- Mostrar su `id`.
- Mostrar su GECOS.
- Mostrar la ruta a su carpeta personal.
- Mostrar el valor de la variable $PS1 de su entorno (si existe).

# Script 02 : Construyendo un pequeño workspace

Haz un script de python que haga las siguientes operaciones:

- Si existe la carpeta `investigacion` en el $HOME del usuario que lanza el script que pregunte si quiere borrarla y debe borrarla si le contesta que sí. En caso de que no se borre, que se detenga la ejecución del script.
- Crear la carpeta `investigacion`.
- Dentro de la carpeta, crear otra llamada `sospechosos` y otra `investigados`.
- Dentro de sospechosos, cread un fichero por cada uno de los alumnos de la clase.
- Ese fichero debe tener como extensión : `.tkn`.
- Escribid en cada uno un número al azar entre el 1 y 6.
- Recorred los ficheros, y todos aquellos que contengan números pares, los enlazáis
  mediante enlaces simbólicos a `investigados`.  (Rutas absolutas o Relativas).
