---
title: Unit 02 - Python Installer
subtitle: "Unit 02"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [ASIR, ASO]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Tarea 1-6

Haz un script en Python que compruebe que el usuario que lo ejecuta tiene
privilegios de `root` y en caso contrario que acabe, mostrando un mensaje de error.

A continuación que compruebe que el paquete `itaca` está instalado en el sistema.

En caso de esté instalado, debe preguntar si desea actualizarlo o reinstalarlo, en caso afirmativo, deberá descargarlo del repositorio de focal de lliurex e instalarlo. En el caso de que no se encuentre instalado, que directamente lo instale.

Algunas pistas de lo que hemos visto en clase:

- Se puede descargar el fichero `Packages.gz` en la carpeta `/tmp` para descomprimirlo.
- Dentro están todas las **URLs** de los paquetes *dentro* del repositorio.
- Tocara ir *línea a línea* buscandolo.

Adjunto un esqueleto y algunos valores de interés.

```python
#!/usr/bin/python3

import sys
import subprocess
# Si no os gusta subprocess podeis
# usar otra libreria de acceso a comandos
# de la Shell.

# Variables utiles dentro del Script

mirror_url = "http://lliurex.net"
distro = "focal"
pacakges_gz_file=mirror_url+"/"+distro+"/dists/focal-updates/main/binary-amd64/Packages.gz"
package_name="itaca"

#
# Do Stuff here ^_^
#

sys.exit(0)

```
