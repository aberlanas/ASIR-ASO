#+Title: Telegram Bot para Sysadmins
#+Author: Angel Berlanas Vicente
#+EXPORT_FILE_NAME: ../PDFS/Tarea-04-TelegramBot.pdf
#+LATEX_COMPILER: xelatex
#+LATEX_HEADER: \hypersetup{colorlinks=true,linkcolor=blue}
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \fancyhead{} % clear all header fields
#+LATEX_HEADER: \pagestyle{fancy}
#+LATEX_HEADER: \fancyhead[R]{2-ASIR: ASO}
#+LATEX_HEADER: \fancyhead[L]{Telegram Bot}
#+LATEX_HEADER: \usepackage{wallpaper}
#+LATEX_HEADER: \ULCornerWallPaper{0.9}{../rsrc/logos/header_europa.png}
#+LATEX_HEADER: \CenterWallPaper{0.7}{../rsrc/logos/watermark_1.png}
#+LATEX_HEADER: \usepackage{fontspec}

#+LATEX: \newpage
* Creación de un Bot en Python

** Introducción

Vamos a crear durante 2 sesiones un Bot en Python que realice bajo demanda una serie de operaciones
de Administración en la máquina en la que está instalado y ejecutándose.

** Librería

Para la creación del Bot vamos a usar la libreria de **python-telegram-bot** que nos ofrece un montón
de posibilidades desde el principio.


[[https://docs.python-telegram-bot.org/en/stable/index.html][[ LINK ] Libreria python-telegram-bot]]


Esta libreria propone una implementación pura en Python a la Libreria de Telegram (API).

: This library provides a pure Python,
: asynchronous interface for the Telegram Bot API.
: It’s compatible with Python versions 3.8+.

#+CAPTION: Página del proyecto
#+NAME:   fig:Telegrambot
[[./imgs/telegrambot-01.png]]

#+LATEX: \newpage
** BotFather

Esto lo rellenaremos en clase.

#+LATEX: \newpage
** Tarea 01: Creación del bot

Usando BotFather conseguid un **token** de seguridad y poned en marcha un **bot**,
que responda a las siguientes órdenes.

*** /info

Muestre información del bot

*** /host-info

Muestre información del Sistema Operativo donde se está ejecutando.

*** /net-info

Muestre la IP(s) de la máquina en la que se está ejecutando el bot.

*** /ping [IP]

Realice un ping a la IP indicada y devuelva si ha sido posible realizarlo.






** Tarea 02: Bot Votado

*** /error_log NUM_ERRORES

Devuelve los ultimos num_errores ~/var/log/syslog~, es que 
aparezca en mayúsculas o minúscula la palabra "error".

*** /service-is-running SERVICE

Devuelve el estado del servicio, si no existe que diga 
que no esiste el servicio.

*** /service-start SERVICE

Enciende el servicio, si no existe que lo diga

*** /service-stop SERVICE

Apaga el servicio, si no existe que lo diga

*** /ports-in-use 

Devuelve el listado de puertos y programas que los están usando.

*** /nmap RED

Lista las IPs accesibles desde esa red y si podéis el nombre del equipo.

*** [ Para el 13 ] /statistics

Devuelve en formato resultados de la encuesta el porcentaje de 
uso de cada uno de los comandos del bot, reiniciando los contadores
cuando se pida el informe.





