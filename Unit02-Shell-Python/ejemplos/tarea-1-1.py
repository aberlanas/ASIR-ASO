#!/usr/bin/python3

import sys
import os

print( " Recorramos el fichero /etc/passwd ")

# En este caso solo como lectura
f = open("/etc/passwd",'r')

# Y ahora entramos en fichero y vamos avanzando
# Una manera elegante de hacerlo es mediante un 
# pequeño bucle While

# Nos apuntamos la linea en la que nos encontramos
numLinea = 0

for linea in f:

    # Incrementamos el numero de la linea
    numLinea=numLinea+1
     
    #print(" La linea "+str(numLinea)+" es : "+linea.strip())   
    # Porque en la linea de arriba se hace el linea.strip()?

    # Vamos ahora a quedarnos con el campo UID
    arrayLinea = linea.split(':')
    uid = arrayLinea[2]

    # Lo convertimos a entero
    uid = int(uid)

    # Y ahora lo comprobamos
    if uid > 100 and uid < 500:
        print (" * login : "+ arrayLinea[0]+ " ----> "+arrayLinea[2])

f.close()

sys.exit(0)
