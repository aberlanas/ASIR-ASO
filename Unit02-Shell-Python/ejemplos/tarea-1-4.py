#!/usr/bin/python3

import sys
import netifaces

print(netifaces.interfaces())

for interfaz in netifaces.interfaces():
    print(" * Interfaz "+interfaz)
    print(" -- IP : "+str(netifaces.ifaddresses(interfaz)))


sys.exit(0)
