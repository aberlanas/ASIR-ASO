---
title: "[ ASO ] Usuarios y Dominios"
author: [Angel Berlanas Vicente]
date: 
subject: "Proyectos 2025"
keywords: [IP, mask]
subtitle: Unit 06
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

\tableofcontents

\newpage
# Introducción

Este documento tiene como propósito servir de guia para el desarrollo de los proyectos
que tendrán lugar como Unidad 06 en *Administración de Sistemas Operativos*.

La idea general es ir añadiendo documentación y recursos en cada uno de los apartados 
con el objetivo de que el alumnado tenga un lugar centralizado, así como diferentes reflexiones y recetas que puedan ser de utilidad para el desarrollo de la idea. 

\newpage

# Proyectos Asignados

| Alumno | Proyecto |
|--------|----------|
| Victor | Windows Server 2025|
| Santi  | FreeIPA |
|Arturo  | Samba en Ubuntu Server|
|Alex    | OpenLDAP, Owncloud y clientes locales|
|Diego   | FreeIPA |
|Sergio  | Samba Domain Controller en CentOS |
|Flor    | Zentyal|
|MarcOS  | Arch, OpenLDAP y kerberos y NFS |
|Javier  | Samba4 + OpenLDAP |
|Daniel  | POSIX identities out of OAuth2 identity providers: how to redesign SSSD and Samba?|


\newpage
# Windows Server 2025


\newpage
# FreeIPA

## Página del Proyecto

* [FreeIPA](https://www.freeipa.org/page/Main_Page)

## Descripción de la tecnología



## Guia de Instalación Oficial

* [ Guía de Instalación ](https://www.freeipa.org/page/Quick_Start_Guide)

\newpage
# Samba 4

## Página del Proyecto

* [ Samba ](https://wiki.samba.org/index.php/Main_Page)
