---
<<<<<<< HEAD
title: "[ ASIR ] The Cups Wars : ASO"
author: [Profes de ASIR]
date: 
subject: "No hay Magenta"
=======
title: "[ ASIR ] The Cups Wars"
author: [Profes de ASIR]
date: 
subject: "No hay Magenta v2"
>>>>>>> a0aada8 ( Limpiando cosas)
keywords: [IP, mask]
subtitle: Unit 06
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

\tableofcontents

\newpage
# Introducción

En esta práctica el alumnado tendrá que poner en juego todo lo aprendido 
durante el curso. 

Tres módulos se ven implicados en la realización de esta práctica conjunta.

- Implantación de Aplicaciones Web.
- Seguridad y Alta Disponibilidad.
- Administración de Sistemas Operativos.

El objetivo: comprobar que el alumnado es capaz de interrelacionar contenidos
de los tres módulos y ha adquirido las destrezas necesarias para resolver problemas 
<<<<<<< HEAD
y construir soluciones complejas en las que intervienen multitud de servicios y tecnologías.
=======
<<<<<<<< HEAD:Unit06-Projects/TheCUPSWARS.md
y construir soluciones complejas con multitud de servicios y tecnologías interrelacionadas.
========
y construir soluciones complejas en las que intervienen multitud de servicios y tecnologías.
>>>>>>>> a0aada8 ( Limpiando cosas):Unit06-Projects/TheCUPSWARS-ASO.md
>>>>>>> a0aada8 ( Limpiando cosas)

\newpage
# Problema a resolver

## No hay Magenta

La malvada corporación *Magenta* ha descubierto al equipo Ingenieros Rebeldes, la última
esperanza de la humanidad de que la Terraformación de Venus no se convierta en una 
<<<<<<< HEAD
tecnocrácia dominada por Microsoft-Amazon-Google-Tesla.
=======
tecnocracia dominada por Microsoft-Amazon-Google-Tesla.
>>>>>>> a0aada8 ( Limpiando cosas)

En la subestación de Terraformación *Dahlton*, los equipos de la Red de Ingenieros 
necesitan exportar los diferentes trabajos que están realizando en multitud de aplicaciones
a un *servidor documental abierto* que será replicado por *la Alianza GNU*, y salvará el conocimiento acumulado durante décadas por estos hombres y mujeres valientes.

<<<<<<< HEAD
Sin embargo, el tiempo se acaba, la corporación Magenta ha enviado a sus agentes 
a Venus, en apenas 6 horas la subestación desaparecido. 
=======
Sin embargo, el tiempo se acaba, la corporación Magenta ha enviado a sus agentes a Venus. En apenas 6 horas, la subestación desaparecerá. 
>>>>>>> a0aada8 ( Limpiando cosas)

¿Seréis capaces de subirlo todo al gestor y protegerlo mediante una **DMZ** para que 
los Agentes de *la Alianza* puedan replicarlo?.


<<<<<<< HEAD
## Requisitos de ASO
=======
# Requisitos de ASO
>>>>>>> a0aada8 ( Limpiando cosas)

Debido a la gran diversidad de aplicaciones que los ingenieros están utilizando para 
trabajar con los grandes proyectos de Terraformación, se hace muy complicado elaborar
un fichero de intercambio que sea compatible con todos ellos, así que se decidió 
que el formato "PDF" sería el ideal.

Los Sistemas Operativos de los Ingenieros son muchos y de lo más variado, así que se propuso
montar un Servidor Ubuntu 24.04 con el servicio CUPS que permitiera a los equipos de la Red 
poder imprimir. Esta impresión se haría de manera anónima en el servidor, en una impresora 
pdf virtual, que imprimirá en pdfs los ficheros que se le envien y que servirá mediante
`nfs` en una carpeta a otro servidor (*domo.venus.2025.net*). 
Aprovecharemos que este servidor (*gutenberg.venus.2025.net*)
estará en la Red LAN de la *subestación* para configurar el servicio MariaDB para almacenar los 
datos sensibles de los ingenieros.


\newpage
## Servidores

<<<<<<< HEAD
### gutenberg.venus.2025.net

#### Sistema Operativo 

Ubuntu 24.04 

#### Servicios
=======
## gutenberg.venus.2025.net

### Sistema Operativo 

Ubuntu 24.04 

### Servicios
>>>>>>> a0aada8 ( Limpiando cosas)

* OpenSSH
* CUPS Server
* MariaDB
* NFS Server
<<<<<<< HEAD


### domo.venus.2025.net

#### Sistema Operativo 

Ubuntu 24.04 

#### Servicios
=======
* NTP


## domo.venus.2025.net

### Sistema Operativo 

Ubuntu 24.04 

### Servicios
>>>>>>> a0aada8 ( Limpiando cosas)

* OpenSSH
* Apache
* NTP

<<<<<<< HEAD
### Script de testeo
=======
## Script de testeo
>>>>>>> a0aada8 ( Limpiando cosas)

Cread un Shell Script que acepte una IP como primer argumento y un servicio como segundo 
de los de la lista siguiente:

| Servicio                 | 
|:------------------------:|
| `cups`                   |
| `nfs-kernel-server`      |
<<<<<<< HEAD
| `nfs-client`             |
| `mariadb`                |
| `apache`                 |
| `openssh-server`         |
| `ntp`                    |
=======
| `mariadb`                |
| `apache`                 |
| `openssh-server`         |
| `ntp`         |
>>>>>>> a0aada8 ( Limpiando cosas)

Si la IP indicada es una de las que la máquina en la que se está ejecutando el script tiene
asignada, además de comprobar el servicio via puerto, es conveniente que compruebe que el 
servicio se encuentra en ejecución en el sistema. En caso de que la IP sea remota, 
que compruebe si el puerto está accesible y/o acepta peticiones.

Si el/la estudiante considera necesario/oportuno ampliar la funcionalidad del Script, comprobando
si se están exportando las carpetas adecuadas, que la base de datos contesta, etc. se tendrá
en cuenta de manera positiva en la nota.

Este script es especialmente útil para comprobar que los mecanismos que se pondrán en marcha
en la DMZ están funcionando.

<<<<<<< HEAD
#### Ampliación
=======
### The CUPS-DMZ situation

En el caso (que es el que está ocurriendo en el examen) de que no tengamos ningún cliente
gráfico desde el que podamos imprimir en `gutenberg`, para el examen configurarlo como 
si hubiera, y cuando haya que crear un nuevo plano, simplemente usad `cp` para duplicar 
un *pdf* en la carpeta que se está *exportando* en `gutenberg.venus.2025.net`.

### Ampliación
>>>>>>> a0aada8 ( Limpiando cosas)

Ampliar el script para que en vez de dos argumentos acepte 2+ y que recorra todos los servicios
que se le indiquen y haga las comprobaciones pertinentes para cada uno de ellos.

<<<<<<< HEAD
# Entrega
=======
## Entrega
>>>>>>> a0aada8 ( Limpiando cosas)

- Fichero de configuración de CUPS.
- Fichero de configuración de la Impresora.
- Fichero de configuración del NFS-Server
- Script de comprobación de servicios.

<<<<<<< HEAD


=======
## Pista para testear si el CUPS funciona 

```shell
timeout 1 bash -c "</dev/tcp/IPQUETOCA/631" &>/dev/null
```

---

\newpage
# Requisitos de SAD 

Debéis crear una estructura para la organización tal y como se muestra en la siguiente imagen:

![DMZ](./imgs/esquemaDMZ.png){width=18cm}

En este caso no hará falta entregar el proyecto de GNS3, sino que tendréis que indicar cada una de las direcciones IP que aparecen 
en la imagen para poder después comprobar si las reglas del firewall son correctas o no. El proyecto en GNS3 os valdrá para poder
realizar comprobaciones.

### Aspectos a tener en cuenta para las direcciones de red:

Para la *LAN* habrá que indicar una dirección y máscara que permita identificar por la IP los siguientes grupos:

- Servidores
- Departamento TIC (al cuál pertenece el equipo PCAdmin)
- Depto 1
- Depto 2
- Depto 3

Para la *DMZ* habrá que indicar una dirección y máscara que consideréis apropiada.

Habrá que especificar la dirección IP de cada una de las interfaces del router. Si en vuestra máquina virtual tienen otros nombres
debéis especificarlo.

### Especificaciones para el iptables

Únicamente se tendrá que permitir lo estrictamente necesario para el funcionamiento de los servidores Domo y Gutenberg así como lo que se especifica a continuación:

- La política por defecto para todas las cadenas será **DROP**
- Se debe realizar un **enmascaramiento** tanto de la *LAN* como de la *DMZ* hacia el exterior, de manera que todos los equipos salgan con la IP pública del router.
- No se permitirá **NADA** desde la *DMZ* hacia la *LAN* excepto lo estrictamente necesario para la comunicación entre los servidores *Domo* y *Gutenberg* y lo que se especifique explícitamente a continuación.
- Todos los equipos de la *LAN* tendrán permitido navegar por páginas web.
- Los equipos del *Depto1* tendrán permitido consultar el correo electrónico *POP* e *IMAP*.
- Los equipos del *Depto2* podrán realizar consultas a servidores *FTP* externos.
- Cualquier equipo externo a la organización podrá acceder al servicio web ofrecido por *Domo* hacia el exterior.
- Los servidores de la *DMZ* deben poder actualizarse mediante *apt*.
- El servidor web de *Domo* debe aceptar peticiones desde la *LAN* únicamente del PC del sysadmin (PCAdmin)
- El servidor *NTP* de *Domo* aceptará consultas tanto de la *LAN* como del exterior.
- El sysadmin de la organización tiene un equipo con IP fija en Internet **192.168.4.15**. Debe poder conectarse por *SSH* a su PC en la *LAN*
- El sysadmin debe poder conectarse por *SSH* desde su PC en la *LAN* a cualquier servidor de la *DMZ*.
- El sysadmin debe poder conectarse por *SSH* desde su PC en la *LAN* al router para administrarlo.
- El sysadmin debe poder hacer **pings** a los servidores de la *DMZ*, al propio router y a cualquier dirección externa.
- El propio router podrá hacer **pings** hacia el exterior.

# Entrega

- Imagen con el esquema de GNS3 con todas las direcciones que se indican rellenadas.
- Fichero .sh con lo necesario para limpiar las reglas de **iptables** actuales y aplicar todas las que se especifican en el enunciado.

\newpage
# Requisitos de IAW  

Para asegurar que los planos y documentos generados en la subestación *Dahlton* sean accesibles de manera organizada y segura, se implementará una aplicación web que permita gestionar estos archivos a través de un CRUD.  

Esta aplicación web deberá cumplir con los siguientes requisitos:  

### **1- Tecnologías a utilizar**  
- **Servidor web**: Apache  
- **Lenguaje de programación**: PHP  
- **Base de datos**: MariaDB (ya desplegado en `gutenberg.venus.2025.net`)  

### **2- Base de datos**  
Los alumnos deberán diseñar la base de datos necesaria para gestionar los planos en PDF. Esta deberá incluir, como mínimo:  
- **Una tabla para almacenar la información de los PDFs.**  
- **Una tabla para los ingenieros responsables.**  
- **Una tabla para los proyectos a los que pertenecen los PDFs.**  

Las relaciones entre estas tablas deben permitir asociar cada PDF con un único ingeniero y un único proyecto.  

**!!IMPORTANTE!!:** La conexión a la Base de Datos desde PHP se realizará mediante **MySQLi Orientado a Objetos**

### **3- Funcionalidades del CRUD**  

La aplicación permitirá realizar las siguientes acciones sobre los PDFs almacenados en el servidor:  

1. **Crear** un nuevo registro de PDF, asignándole un nombre, una ruta de almacenamiento, un ingeniero responsable y un proyecto.  

2. **Leer** la lista de PDFs almacenados, mostrando en pantalla:  
   - **Nombre del archivo**  
   - **Nombre del ingeniero responsable**  
   - **Nombre del proyecto asociado**  
   - **Botón "Descargar"** que permitirá acceder al archivo para su descarga.  
  
3. **Actualizar** los datos de un PDF ya registrado.

4. **Eliminar** un PDF de la base de datos.  

### **4- Requisitos de la interfaz web**  
- La lista de PDFs debe mostrarse en una tabla HTML+CSS.  
- La columna que almacene la **ruta** del archivo no debe mostrarse en la interfaz. En su lugar, se incluirá un botón " [ ICONO ] Descargar" que enlazará directamente al archivo.  
- Se debe implementar **validación básica** en los formularios (por ejemplo, que no haya campos vacíos).  
- No debe aparecer ningun ID en la interfaz, unicamente decriptivos (nombre, descripción... entendibles para el usuario final)
  
### **5- Entregables**  
- Ruta al repositorio donde se encuentra el código fuente del CRUD en PHP+HTML+Css.  
- Script SQL con la estructura de la base de datos y datos de ejemplo.  
- Capturas de pantalla de la aplicación funcionando.  

---
>>>>>>> a0aada8 ( Limpiando cosas)
