---
title: "[ ASIR ] The Cups Wars : SAD"
author: [Profes de ASIR]
date: 
subject: "SAD"
keywords: [IP, mask]
subtitle: Unit 06
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...



## Requisitos de SAD 

Debéis crear una estructura para la organización tal y como se muestra en la siguiente imagen:

<<<<<<< HEAD
![DMZ](./imgs/esquemaDMZ.png){width=18cm}
=======
![](./imgs/esquemaDMZ.png){width=18cm}
>>>>>>> a0aada8 ( Limpiando cosas)

En este caso no hará falta entregar el proyecto de GNS3, sino que tendréis que indicar cada una de las direcciones IP que aparecen 
en la imagen para poder después comprobar si las reglas del firewall son correctas o no. El proyecto en GNS3 os valdrá para poder
realizar comprobaciones.

### Aspectos a tener en cuenta para las direcciones de red:

Para la *LAN* habrá que indicar una dirección y máscara que permita identificar por la IP los siguientes grupos:

- Servidores
- Departamento TIC (al cuál pertenece el equipo PCAdmin)
- Depto 1
- Depto 2
- Depto 3

Para la *DMZ* habrá que indicar una dirección y máscara que consideréis apropiada.

Habrá que especificar la dirección IP de cada una de las interfaces del router. Si en vuestra máquina virtual tienen otros nombres
debéis especificarlo.

### Especificaciones para el iptables

Únicamente se tendrá que permitir lo estrictamente necesario para el funcionamiento de los servidores Domo y Gutenberg así como lo que se especifica a continuación:

- La política por defecto para todas las cadenas será **DROP**
- Se debe realizar un **enmascaramiento** tanto de la *LAN* como de la *DMZ* hacia el exterior, de manera que todos los equipos salgan con la IP pública del router.
- No se permitirá **NADA** desde la *DMZ* hacia la *LAN* excepto lo estrictamente necesario para la comunicación entre los ervidores *Domo* y *Gutenberg* y lo que especifique explícitamente a continuación.
- Todos los equipos de la *LAN* tendrán permitido navegar por páginas web.
- Los equipos del *Depto1* tendrán permitido consultar el correo electrónico *POP* e *IMAP*.
- Los equipos del *Depto2* podrán realizar consultas a servidores *FTP* externos.
- Cualquier equipo externo a la organización podrá acceder a los servicios ofrecidos por *Domo* hacia el exterior.
- Los servidores de la *DMZ* deben poder actualizarse mediante *apt*.
- El servidor *NTP* de *Domo* aceptará consultas tanto de la LAN como del exterior.
- El sysadmin de la organización tiene un equipo con IP fija en Internet **192.168.4.15**. Debe poder conectarse por *SSH* a su PC en la *LAN*
- El sysadmin debe poder conectarse por *SSH* desde su PC en la *LAN* a cualquier servidor de la *DMZ*.
- El sysadmin debe poder conectarse por *SSH* desde su PC en la *LAN* al router para administrarlo.
- El sysadmin debe poder hacer **pings** a los servidores de la *DMZ*, al propio router y a cualquier dirección externa.
- El propio router podrá hacer **pings** hacia el exterior.

# Entrega

- Imagen con el esquema de GNS3 con todas las direcciones que se indican rellenadas.
- Fichero .sh con lo necesario para limpiar las reglas de **iptables** actuales y aplicar todas las que se especifican en el enunciado.
