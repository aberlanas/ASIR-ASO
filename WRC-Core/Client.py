#!/usr/bin/env python3

import sys
import os
import pwd
import time
import socket
from libwrc import *
from datetime import datetime


HOST = "127.0.1.1"  # The server's hostname or IP address
PORT = 2222  # The port used by the server
component="IRC Client"
username = str(pwd.getpwuid(os.getuid())[0])
hostname = socket.gethostname()
enviado = False

wrc_log(component,"Welcome to the Worthington Awesome Chat [WAC]")
wrc_log(component,"Iniciando ... (exit to quit)")

list_of_msg = []

while True:


	mensaje = input("["+get_date()+"] <"+username+">: ")
	enviado = False


	if mensaje == "/exit-client" :
		break
	if mensaje == "/list-gpg-keys":
		# Mostrar todas las claves publicas que tengo disponibles
		# para enviar al servidor para que el cifre de manera asimetrica
		# los mensajes que em envia
		pass


	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		conectado = True
		try:
			s.connect((HOST, PORT))
			
		except ConnectionRefusedError as e:
			wrc_error(component,str(e))
			conectado = False
			pass
			
		if conectado and not enviado:
			#DEBUG
			s.sendall(build_msg(username,hostname,mensaje))
			enviado= True
			try:
				data = s.recv(1024)
				#if str(data) == " Parando el servidor ":
				#	wrc_log(component," Parando el Cliente")
				#	s.close()
				#	sys.exit()
			except ConnectionRefusedError as e:
				wrc_error(component,str(e))
		else :
			wrc_error(component," * El servidor parece apagado ")
			

	if conectado:
		print("["+get_date()+"] <rcvd>: "+str(data))
		#list_of_msg.append(mensaje)
		#print(list_of_msg)

print(" * [ Client ] : Exiting ... ")
sys.exit(0)