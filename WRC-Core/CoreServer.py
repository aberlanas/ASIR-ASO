#!/usr/bin/env python3

import socket
import sys
import time
from datetime import datetime
from libwrc import *

HOST = "127.0.1.1"  # Standard loopback interface address (localhost)
PORT = 2222  # Port to listen on (non-privileged ports are > 1023)
component="CoreServer"
notExit = True

print(" * [ CoreServer ] : Starting")

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.bind((HOST, PORT))
	wrc_log(component,"Listening at "+HOST+":"+str(PORT))

	while True and notExit:
		s.listen()
		conn, addr = s.accept()
		with conn:
			wrc_log(component,"Connected by " +str(addr))
			while True:
				data = conn.recv(1024)
				
				# Ojito que hay cosas en la conexion
				if not data:
					break
					
				
				ms = debuild_msg(data)[1]
				info = debuild_msg(data)[0]
				wrc_log(component,info+" desde [ "+str(addr)+"]")
				wrc_warn(component,ms)

				if (ms == "/stop-server"):
					wrc_warn(component," Parando el servidor")
					conn.sendall(b" Parando el servidor ")
					s.close()
					sys.exit(0)
					notExit = False
					break

				conn.sendall(b"ACK")
				

