# WRC - Warren Relay Chat

En Terraformadores se han dado cuenta de que necesitan una herramienta
desarrollada *Ad-Hoc* para permitir la comunicación de sus técnic@s entre ell@s
y además interactuar entre las diferentes máquinas que proveerán la infraestructura.

A lo largo de este proyecto se introducirán los conceptos que cualquier *SysAdmin* que se precie debe conocer.

## Paso 00 : Cliente y Servidor

Se deben crear dos ficheros Python que formarán la estructura *básica*:

### CoreServer.py

```python

#!/usr/bin/env python3

import socket
import sys
import time
from datetime import datetime
from libwrc import *

HOST = "127.0.1.1"  # Standard loopback interface address (localhost)
PORT = 2222  # Port to listen on (non-privileged ports are > 1023)
component="CoreServer"
notExit = True

print(" * [ CoreServer ] : Starting")

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.bind((HOST, PORT))
	wrc_log(component,"Listening at "+HOST+":"+str(PORT))

	while True and notExit:
		s.listen()
		conn, addr = s.accept()
		with conn:
			wrc_log(component,"Connected by " +str(addr))
			while True:
				data = conn.recv(1024)
				
				# Ojito que hay cosas en la conexion
				if not data:
					break
					
				
				ms = debuild_msg(data)[1]
				info = debuild_msg(data)[0]
				wrc_log(component,info+" desde [ "+str(addr)+"]")
				wrc_warn(component,ms)

				if (ms == "/stop-server"):
					wrc_warn(component," Parando el servidor")
					conn.sendall(b" Parando el servidor ")
					s.close()
					sys.exit(0)
					notExit = False
					break

				conn.sendall(b"ACK")
```

# Empaquetado

## Windows

Vamos a comenzar con la instalación en un Windows de nuestro software a ver cómo funciona. Toca averiguar dependencias, 
instalación de Python3 en Windows, etc.

## Ubuntu

Esto lo haremos en clase.

# ROADMAP

* Comprobar que el servidor está conectado antes de enviar un mensaje.
* Leer información de configuración de dos ficheros:
   - `ClientInfo.yaml`
   - `ServerInfo.yaml`
* Enviar información acerca de la versión antes de comenzar la comunicación entre las partes (Véase [ASO8.md](./ASO8.md)).
* Mostrar mensajes de error en caso de que no sea la misma versión (permitiendo la actualización).
* `/help` -> Mostrar todas las opciones disponibles del cliente.
* `/list-gpg-keys` -> Muestra las claves disponibles para ser enviadas al servidor.
* `/install-gpg-key "basename.gpg"` -> Accede al directorio de claves indicado en el fichero de configuración y envia e instala
  la clave en el servidor, y el servidor contesta con su clave propia para que a partir de este momento los mensajes vayan
  cifrados entre ellos usando cifrado **ASIMETRICO**.
* `/stop-server` -> Detiene el servidor y luego cierra el cliente *elegantemente*.
* `/start-encrypt` -> Genera un cifrado **SIMETRICO**, lo negocia con el server y comienza a usarlo para la conversación.
* `/stop-encrypt` -> Deja de usar el cifrado **SIMÉTRICO** para la comunicación.
* `/exit-client` -> Sale del cliente.
* Enviar información entre el cliente y el servidor en el formato adecuado (Véase [ASO8.md](./ASO8.md)).




# WRComands

| Comando | Version | Función |
|--------- |---------|---------|
| `/help` | >0.1    | Muestra la ayuda|
| `/stop-server` | >0.1 | Para el Servidor |
| `/exit-client` | >0.1 | Sale del cliente |
| `/list-gpg-keys` | >0.1 | Lista Claves GPG Disponibles |
| `/install-gpg-key "basename.gpg"` | >0.1 | Instala claves publicas en el servidor |
| `/start-encrypt` | >0.2 | Negocia y usa cifrado simétrico |
| `/stop-encrypt` | >0.2 | Deja de usar cifrado simétrico |
