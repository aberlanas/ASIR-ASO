# Imports
from termcolor import colored
from datetime import datetime


# Default values

component="default"

# Init WRC Library

def get_date():
	current_time = datetime.now()
	aux=""+str(current_time.time()).split(":")[0]+":"
	aux+=str(current_time.time()).split(":")[1]
	return aux

def build_msg(username,hostname,msg):
	msg="<"+username+"><"+hostname+"><info>:"+msg
	return (str.encode(msg))
	
def debuild_msg(rawmsg):
	msg_list=[]
	msg = rawmsg.decode("utf-8")
	info=msg.split(":")[0]
	msg_list.append(info)
	msg2=msg.split(":")[1]
	msg_list.append(msg2)
	return (msg_list)

def wrc_log(component,msg):
	print(colored(" *** "+component+" ["+get_date()+"] "+msg,"green"))

def wrc_warn(component,msg):
	print(colored(" *** "+component+" ["+get_date()+"] "+msg,"yellow"))

def wrc_error(component,msg):
	print(colored(" *** "+component+" ["+get_date()+"] "+msg,"red"))

def save_msg(component,msg):
	print(" log ")