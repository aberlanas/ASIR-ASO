// question: 0  name: Switch category to $course$/top/Categoria per defecte en 1CFM Sistemes operatius monolloc 22//23   1662712834
$CATEGORY: $course$/top/Categoria per defecte en 1CFM Sistemes operatius monolloc 22//23   1662712834


// question: 0  name: Switch category to $course$/top/Categoria per defecte en 1CFM Sistemes operatius monolloc 22//23   1662712834/Permisos-Modulo-0
$CATEGORY: $course$/top/Categoria per defecte en 1CFM Sistemes operatius monolloc 22//23   1662712834/Permisos-Modulo-0


// question: name: chmod-02
::chmod-02::[html]¿De los siguientes comandos, cual se utiliza para cambiar el propietario de un fichero en sistemas GNU/LinuX?{
	=chown
	~chmod
	~which
	~adduser
}


// question: name: chmod-03
::chmod-03::[html]Si queremos que el usuario propietario tenga todos los permisos y el resto ninguno, ejecutaremos:{
	=chmod 700
	~chmod u+all
	~chmod g:rwx 
	~chmod u:r-x
}


// question: name: chmod-04
::chmod-04::[html]Si queremos que el usuario propietario tenga permisos de escritura y lectura y el resto ninguno, ejecutaremos:{
	=chmod 600
	~chmod u:rwx g:--- o:---
	~chmod 060
	~chmod 006
}

// question: name: chmod-05
::chmod-05::[html]Si queremos que el usuario propietario tenga permisos de lectura y ejecución y el resto ninguno, ejecutaremos:{
	=chmod 500
	~chmod u:rx g:rx o:rx
	~chmod 700
	~chmod 400
}

// question: name: chmod-06
::chmod-06::[html]La opción -c en chmod indica:{
	=Solo mostrará los cambios si los hay.
	~Solo aplicará a los ficheros que sean de tipo carácter.
	~Lee los permisos a aplicar usando entrada estándar.
	~No se puede aplicar  a chmod
}

// question: name: chmod-07
::chmod-07::[html]La opción -R en chmod indica:{
	=Que los permisos se establecerán de manera recursiva en los ficheros y directorios que se contengan en la ruta indicada.
	~Que no pregunte para cambiar los permisos.
	~Resetea los permisos a los valores por defecto.
	~Establece sólo lectura para todos los usuarios.
}

// question: name: chmod-08
::chmod-08::[html]¿Cual de los siguientes comandos establece lectura unicamente?:{
	=chmod 444
	~chmod 555
	~chmod 111
	~chmod 222
}
